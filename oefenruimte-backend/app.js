require('dotenv').config();
const utils = require('./utils');

const express = require('express');
const app = express();
const port = 3000;

const db = utils.db.connectDB();
app.get('/', (req, res) => res.send('Hello World!'));


app.listen(port, () => console.log(`Example app listening on port ${port}!`));