// const MongoClient = import('mongodb').MongoClient;
const mongo = require('mongodb').MongoClient
const path = require('path');

function ConnectDB() {
    let connected = false;


     const url = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@ds351827.mlab.com:51827/oefenruimte_db`;
    mongo.connect(mongo.connect(url, function (err, db) {
        if (err) throw err;
        if (connected) return;
        connected = true;
        console.log("Connecting to db");
        return db;
    })).catch(err => console.log(err));
}

module.exports = {
    connectDB: ConnectDB
};